<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html >
<html lang="en">
<head>
    <title>Main</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <a class="btn btn-default" href="${pageContext.request.contextPath}/main"
               type="button">Choice page</a>
            <h4>Table content</h4>
        </div>
        <div class="col-md-4"></div>
    </div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form:form modelAttribute="personslist" action="${pageContext.request.contextPath}/persons/update" method="post">
                <table id="myTable" class="table table-striped ">
                  <thead>
                    <tr>
                        <th></th>
                        <th><a href="${pageContext.request.contextPath}/persons/show/sorted?name=name">Name</a></th>
                        <th><a href="${pageContext.request.contextPath}/persons/show/sorted?name=surname">Surname</a></th>
                        <th><a href="${pageContext.request.contextPath}/persons/show/sorted?name=age">Age</a></th>
                        <th><a href="${pageContext.request.contextPath}/persons/show/sorted?name=position">Position</a></th>
                        <th>Action</th>
                    </tr>
                  </thead>

                 <c:choose>
                        <c:when test="${empty personslist}">
                            <tr>
                                <td colspan="7">
                                    There are no persons
                                </td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                    <tbody>
                            <c:forEach var="person" varStatus="status" items="${personslist.persons}">
                                <tr>
                                    <td><input  type="hidden" name="persons[${status.index}].id" value="${person.id}"/></td>
                                    <td><input  name="persons[${status.index}].name" value="${person.name}" type="text"/></td>
                                    <td><input  name="persons[${status.index}].surname" value="${person.surname}" type="text"/></td>
                                    <td><input name="persons[${status.index}].age" value="${person.age}" type="number" min="1" step="1"/></td>
                                    <td><input name="persons[${status.index}].position" value="${person.position}" type="text"/></td>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/persons/delete?id=${person.id}">Delete</a>
                                    </td>
                                </tr>
                            </c:forEach>
                    </tbody>
                        </c:otherwise>
                    </c:choose>

                </table>
                <a class="btn btn-default" href="${pageContext.request.contextPath}/persons/add"
                   type="button">Add</a>
                <c:if test="${not empty personslist}">
                    <button type="submit" class="btn btn-default">Save</button>
                </c:if>
            </form:form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>