<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html >
<html lang="en">
<head>
    <title>Main</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h4>Enter information, please</h4>
        </div>
        <div class="col-md-4"></div>
    </div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form:form action="${pageContext.request.contextPath}/persons/add" modelAttribute="person" method="post">
                <div class="form-group">
                    <form:input type="hidden" value="${id}" cssClass="form-control" id="id" path="id" name="id"
                                />
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <p class="bg-danger"><form:errors path="name"/></p>
                    <form:input required="true" type="text" cssClass="form-control" id="name" path="name" name="name"
                                placeholder="Name"/>
                </div>
                <div class="form-group">
                    <label for="surname">Surname</label>
                    <p class="bg-danger"><form:errors path="surname"/></p>
                    <form:input required="true" type="text" path="surname" cssClass="form-control" id="surname"
                                name="surname"
                                placeholder="Surname"/>
                </div>
                <div class="form-group">
                    <label for="age">Age</label>
                    <p class="bg-danger"><form:errors path="age"/></p>
                    <form:input required="true" type="number" min="0" step="1" path="age" cssClass="form-control"
                                id="age"
                                name="age"
                    />
                </div>
                <div class="form-group">
                    <label for="position">Position</label>
                    <p class="bg-danger"><form:errors path="position"/></p>
                    <form:input required="true" type="text" path="position" cssClass="form-control" id="position"
                                name="position"
                                placeholder="Position"/>
                </div>


                <button type="submit" class="btn btn-default">Add</button>
            </form:form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>