<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html >
<html lang="en">
<head>
    <title>Main</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-4">
            <a class="btn btn-default" href="${pageContext.request.contextPath}/main"
               type="button">Choice page</a>
            <h4>Table content</h4>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form:form modelAttribute="filmscatalog" action="${pageContext.request.contextPath}/films/update" method="post">
                <table id="myTable" class="table table-striped">
                    <tr>
                        <th></th>
                        <th><a href="${pageContext.request.contextPath}/films/show/sorted?name=name">Name</a></th>
                         <th><a href="${pageContext.request.contextPath}/films/show/sorted?name=producer">Producer</a></th>
                        <th><a href="${pageContext.request.contextPath}/films/show/sorted?name=rate">Rate</a></th>
                        <th><a href="${pageContext.request.contextPath}/films/show/sorted?name=description">Description</a></th>
                        <th>Action</th>
                    </tr>
                    <c:choose>
                        <c:when test="${empty filmscatalog}">
                            <tr>
                                <td colspan="7">
                                    There are no films
                                </td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="film" varStatus="status" items="${filmscatalog.films}">
                                <tr>
                                    <td><input  type="hidden" name="films[${status.index}].id" value="${film.id}"/></td>
                                    <td><input  name="films[${status.index}].name" value="${film.name}" type="text"/></td>
                                    <td><input  name="films[${status.index}].producer" value="${film.producer}" type="text"/></td>
                                    <td><input name="films[${status.index}].rate" value="${film.rate}" type="number" min="1" step="0.01"/></td>
                                    <td><input name="films[${status.index}].description" value="${film.description}" type="text"/></td>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/films/delete?id=${film.id}">Delete</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </table>
                <a class="btn btn-default" href="${pageContext.request.contextPath}/films/add"
                   type="button">Add</a>
                <c:if test="${not empty filmscatalog}">
                    <button type="submit" class="btn btn-default">Save</button>
                </c:if>
            </form:form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>