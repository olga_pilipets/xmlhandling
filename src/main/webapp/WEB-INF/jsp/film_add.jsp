<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html >
<html lang="en">
<head>
    <title>Main</title>
    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h4>Enter information, please</h4>
        </div>
        <div class="col-md-4"></div>
    </div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form:form action="${pageContext.request.contextPath}/films/add" modelAttribute="film" method="post">
                <div class="form-group">
                    <form:input type="hidden" value="${id}" cssClass="form-control" id="id" path="id" name="id"
                    />
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <p class="bg-danger"><form:errors path="name"/></p>
                    <form:input required="true" type="text" cssClass="form-control" id="name" path="name" name="name"
                                placeholder="Name"/>
                </div>
                <div class="form-group">
                    <label for="producer">Producer</label>
                    <p class="bg-danger"><form:errors path="producer"/></p>
                    <form:input required="true" type="text" path="producer" cssClass="form-control" id="producer"
                                name="producer"
                                placeholder="producer"/>
                </div>
                <div class="form-group">
                    <label for="rate">Rate</label>
                    <p class="bg-danger"><form:errors path="rate"/></p>
                    <form:input required="true" type="number" min="0" step="0.01" path="rate" cssClass="form-control"
                                id="rate"
                                name="rate"
                    />
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <p class="bg-danger"><form:errors path="description"/></p>
                    <form:input required="true" type="text" path="description" cssClass="form-control" id="description"
                                name="description"
                                placeholder="description"/>
                </div>


                <button type="submit" class="btn btn-default">Add</button>
            </form:form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>