package application.web.utilComparator;

import application.repository.model.Person;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class PersonComparator {

    public static List<Person> nameSort(List<Person> persons) {
        Collections.sort(persons, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        return persons;
    }

    public static List<Person> surNameSort(List<Person> persons) {
        Collections.sort(persons, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getSurname().compareToIgnoreCase(o2.getSurname());
            }
        });
        return persons;
    }

    public static List<Person> ageSort(List<Person> persons) {
        Collections.sort(persons, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getAge().compareTo(o2.getAge());
            }
        });
        return persons;
    }

    public static List<Person> positionSort(List<Person> persons) {
        Collections.sort(persons, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getPosition().compareTo(o2.getPosition());
            }
        });
        return persons;
    }
}
