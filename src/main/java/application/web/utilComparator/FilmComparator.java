package application.web.utilComparator;


import application.repository.model.Film;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FilmComparator {

    public static List<Film> nameSort(List<Film> films) {
        Collections.sort(films, new Comparator<Film>() {
            @Override
            public int compare(Film o1, Film o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        return films;
    }

    public static List<Film> producerSort(List<Film> films) {
        Collections.sort(films, new Comparator<Film>() {
            @Override
            public int compare(Film o1, Film o2) {
                return o1.getProducer().compareToIgnoreCase(o2.getProducer());
            }
        });
        return films;
    }
    public static List<Film> rateSort(List<Film> films) {
        Collections.sort(films, new Comparator<Film>() {
            @Override
            public int compare(Film o1, Film o2) {
                return o1.getRate().compareTo(o2.getRate());
            }
        });
        return films;
    }
    public static List<Film> descriptionSort(List<Film> films) {
        Collections.sort(films, new Comparator<Film>() {
            @Override
            public int compare(Film o1, Film o2) {
                return o1.getDescription().compareToIgnoreCase(o2.getDescription());
            }
        });
        return films;
    }


}
