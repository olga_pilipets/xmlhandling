package application.web.controller;


import application.repository.model.Person;
import application.repository.model.PersonsList;
import application.service.PersonService;
import application.web.utilComparator.PersonComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.List;

@Controller
public class PersonController {
    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @RequestMapping(value = "/persons/show", method = RequestMethod.GET)
    public String showPersons(Model model) {
        PersonsList personsList = personService.getPersons();
        List<Person> persons=personsList.getPersons();
        for (Person person : persons) {
            System.out.println(person.getId()+person.getName());
        }
        model.addAttribute("personslist", personsList);
        return "persons_show";
    }

    @RequestMapping(value = "/persons/delete", method = RequestMethod.GET)
    public String deletePerson(@RequestParam(value = "id") Integer index) {
        personService.deletePerson(index);
        return "redirect:/persons/show";
    }

    @RequestMapping(value = "/persons/update", method = RequestMethod.POST)
    public String updatePersons(@ModelAttribute(value = "personslist") PersonsList personsList) {
        List<Person> persons=personsList.getPersons();
        for (Person person : persons) {
            System.out.println(person.getId()+person.getName());
        }
        personService.addPersons(personsList);
        return "redirect:/persons/show";
    }

    @RequestMapping(value = "/persons/add", method = RequestMethod.GET)
    public String openAddPerson(Model model, @ModelAttribute("person") Person person) {
        Integer id = personService.findMaxId() + 1;
        model.addAttribute("id", id);
        return "person_add";
    }

    @RequestMapping(value = "/persons/add", method = RequestMethod.POST)
    public String addPerson(@ModelAttribute("person") Person person) {
        personService.addPerson(person);
        return "redirect:/persons/show";
    }

    @RequestMapping(value = "/persons/show/sorted", method = RequestMethod.GET)
    public String showPersonsSorted(Model model,
                                    @RequestParam(value = "name") String name) {
        PersonsList personsList = personService.getPersons();
        List<Person> persons = personsList.getPersons();
        if (name.equals("name")) {
            PersonComparator.nameSort(persons);
        } else if (name.equals("surname")) {
            PersonComparator.surNameSort(persons);
        } else if (name.equals("age")) {
            PersonComparator.ageSort(persons);
        } else if (name.equals("position")) {
            PersonComparator.positionSort(persons);
        }
        personsList.setPersons(persons);
        model.addAttribute("personslist", personsList);
        return "persons_show";
    }
}
