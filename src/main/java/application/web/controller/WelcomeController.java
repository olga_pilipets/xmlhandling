package application.web.controller;


import application.repository.model.FileNames;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WelcomeController {

    @RequestMapping(value = {"/", "/main"}, method = RequestMethod.GET)
    public String showWelcomePage(Model model) {
        model.addAttribute("files", FileNames.values());
        return "main";
    }

    @RequestMapping(value = { "/main"}, method = RequestMethod.POST)
    public String showFilePage(@RequestParam(value = "file") String file) {
        if (FileNames.valueOf(file) == FileNames.PERSON) {
            return "redirect:/persons/show";
        } else if (FileNames.valueOf(file) == FileNames.FILM) {
            return "redirect:/films/show";
        } else {
            return "main";
        }
    }
}
