package application.web.controller;

import application.repository.model.Film;
import application.repository.model.FilmsCatalog;
import application.service.FilmService;
import application.web.utilComparator.FilmComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Controller
public class FilmController {

    private final FilmService filmService;

    @Autowired
    public FilmController(FilmService filmService) {
        this.filmService = filmService;
    }

    @RequestMapping(value = "/films/show", method = RequestMethod.GET)
    public String showFilms(Model model) {
        FilmsCatalog filmsCatalog = filmService.showFilms();
        model.addAttribute("filmscatalog", filmsCatalog);
        return "films_show";
    }

    @RequestMapping(value = "/films/delete", method = RequestMethod.GET)
    public String deleteFilm(@RequestParam(value = "id") Integer index) {
        filmService.deleteFilm(index);
        return "redirect:/films/show";
    }

    @RequestMapping(value = "/films/update", method = RequestMethod.POST)
    public String updateFilms(@ModelAttribute(value = "filmscatalog") FilmsCatalog filmsCatalog) {
        filmService.addFilms(filmsCatalog.getFilms());
        return "redirect:/films/show";
    }

    @RequestMapping(value = "/films/add", method = RequestMethod.GET)
    public String openAddFilm(Model model, @ModelAttribute("film") Film film) {
        Integer id = filmService.findMaxId() + 1;
        model.addAttribute("id", id);
        return "film_add";
    }

    @RequestMapping(value = "/films/add", method = RequestMethod.POST)
    public String addFilm(@ModelAttribute("film") Film film) {
        filmService.addFilm(film);
        return "redirect:/films/show";
    }


    @RequestMapping(value = "/films/show/sorted", method = RequestMethod.GET)
    public String showFilmsSorted(Model model,
                                  @RequestParam(value = "name") String name) {
        FilmsCatalog filmsCatalog = filmService.showFilms();
        List<Film> films = filmsCatalog.getFilms();
        if (name.equals("name")) {
            FilmComparator.nameSort(films);
        } else if (name.equals("producer")) {
            FilmComparator.producerSort(films);
        } else if (name.equals("rate")) {
            FilmComparator.rateSort(films);
        } else if (name.equals("description")) {
            FilmComparator.descriptionSort(films);
        }
        filmsCatalog.setFilms(films);
        model.addAttribute("filmscatalog", filmsCatalog);
        return "films_show";
    }

}
