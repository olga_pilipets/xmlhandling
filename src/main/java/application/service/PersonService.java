package application.service;

import application.repository.model.Person;
import application.repository.model.PersonsList;

import java.util.List;


public interface PersonService {
    List<Person> showPersons();

    void addPersons(PersonsList personsList);

    void addPerson(Person person);

    void deletePerson(Integer index);

    PersonsList getPersons();

    Integer findMaxId();
}
