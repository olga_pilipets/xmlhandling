package application.service.impl;

import application.repository.dao.PersonDAO;
import application.repository.dao.PersonsListDAO;
import application.repository.model.Person;
import application.repository.model.PersonsList;
import application.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;


@Service
public class PersonServiceImpl implements PersonService {

    private final PersonDAO personDAO;
    private final PersonsListDAO personsListDAO;

    @Autowired
    public PersonServiceImpl(PersonDAO personDAO, PersonsListDAO personsListDAO) {
        this.personDAO = personDAO;
        this.personsListDAO = personsListDAO;
    }

    @Override
    public List<Person> showPersons() {
        return personDAO.getPersons(personsListDAO.getPersons());
    }

    @Override
    public PersonsList getPersons() {
        return personsListDAO.getPersons();
    }

    @Override
    public void addPersons(PersonsList personsList) {
        personsListDAO.setPersons(personsList);
    }



    @Override
    public void addPerson(Person person) {
        PersonsList personsList=getPersons();
        List<Person> persons=showPersons();
        persons.add(person);
        personsList.setPersons(persons);
        addPersons(personsList);
    }

    @Override
    public void deletePerson(Integer index) {
        List<Person> persons = getPersons().getPersons();
        Iterator<Person> it = persons.iterator();
        while (it.hasNext()) {
            Person person = it.next();
            if (person.getId().equals(index)) {
                it.remove();
            }
        }
        PersonsList personsList=getPersons();
        personsList.setPersons(persons);
        addPersons(personsList);
    }


    @Override
    public Integer findMaxId() {
        List<Person> persons = showPersons();
        Integer maxId = 0;
        for (int i = 0; i < persons.size(); i++) {
            Integer currentId = persons.get(i).getId();
            if (currentId > maxId) {
                maxId = currentId;
            }
        }
        return maxId;
    }



}
