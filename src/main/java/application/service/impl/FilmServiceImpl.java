package application.service.impl;

import application.repository.dao.FilmDAO;
import application.repository.dao.FilmsCatalogDAO;
import application.repository.model.Film;
import application.repository.model.FilmsCatalog;
import application.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;


@Service
public class FilmServiceImpl implements FilmService {

    private final FilmDAO filmDAO;
    private final FilmsCatalogDAO filmsCatalogDAO;

    @Autowired
    public FilmServiceImpl(FilmDAO filmDAO, FilmsCatalogDAO filmsCatalogDAO) {
        this.filmDAO = filmDAO;
        this.filmsCatalogDAO = filmsCatalogDAO;
    }
    @Override
    public FilmsCatalog showFilms() {
        return filmsCatalogDAO.getFilms();
    }
    @Override
    public void addFilms(List<Film> films){
        filmsCatalogDAO.setFilms(filmDAO.setFilms(films));
    }
    @Override
    public void addFilm(Film film){
        List<Film> films=showFilms().getFilms();
        films.add(film);
        addFilms(films);
    }
    @Override
    public void deleteFilm(Integer index){
        List<Film> films=showFilms().getFilms();
        Iterator<Film> it=films.iterator();
        while (it.hasNext()) {
            Film film = it.next();
            if (film.getId().equals(index)) {
                it.remove();
            }
        }
        addFilms(films);
    }
    @Override
    public Integer findMaxId() {
        List<Film> films = showFilms().getFilms();
        Integer maxId = 0;
        for (int i = 0; i < films.size(); i++) {
            Integer currentId = films.get(i).getId();
            if (currentId > maxId) {
                maxId = currentId;
            }
        }
        return maxId;
    }

}
