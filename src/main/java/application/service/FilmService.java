package application.service;

import application.repository.model.Film;
import application.repository.model.FilmsCatalog;

import java.util.List;


public interface FilmService {
    FilmsCatalog showFilms();
    void addFilms(List<Film> films);
    void addFilm(Film film);
    void deleteFilm(Integer index);
    Integer findMaxId();
}
