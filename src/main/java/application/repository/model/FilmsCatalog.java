package application.repository.model;


import java.util.List;


public class FilmsCatalog {
    private List<Film> films;

    public List<Film> getFilms() {
        return films;
    }

    public void setFilms(List<Film> films) {
        this.films = films;
    }
}
