package application.repository.dao;

import application.repository.model.Film;
import application.repository.model.FilmsCatalog;

import java.util.List;


public interface FilmDAO{
       FilmsCatalog setFilms(List<Film> films);

}
