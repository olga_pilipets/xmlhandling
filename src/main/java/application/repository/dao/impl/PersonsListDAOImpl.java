package application.repository.dao.impl;

import application.repository.dao.PersonsListDAO;
import application.repository.model.PersonsList;
import org.springframework.stereotype.Repository;


@Repository
public class PersonsListDAOImpl extends GenericDAOImpl<PersonsList> implements PersonsListDAO {

    public PersonsList getPersons() {

        return getInfo("/persons.xml", "/mapping-persons.xml");
    }

    public void setPersons(PersonsList personsList) {

        addInfo(personsList, "/persons.xml", "/mapping-persons.xml");
    }


}
