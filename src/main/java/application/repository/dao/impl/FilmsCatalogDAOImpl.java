package application.repository.dao.impl;

import application.repository.dao.FilmsCatalogDAO;
import application.repository.model.FilmsCatalog;
import org.springframework.stereotype.Repository;


@Repository("filmsCatalogDAO")
public class FilmsCatalogDAOImpl extends GenericDAOImpl<FilmsCatalog> implements FilmsCatalogDAO {
    public FilmsCatalog getFilms() {
        return getInfo("/film.xml","/mapping-film.xml");
    }
    public void setFilms(FilmsCatalog filmsCatalog){
        addInfo(filmsCatalog,"/film.xml","/mapping-film.xml");
    }
}
