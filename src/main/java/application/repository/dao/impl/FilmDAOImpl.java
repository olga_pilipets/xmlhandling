package application.repository.dao.impl;

import application.repository.dao.FilmDAO;
import application.repository.model.Film;
import application.repository.model.FilmsCatalog;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository ("filmDAO")
public class FilmDAOImpl extends GenericDAOImpl<Film> implements FilmDAO {

    public FilmsCatalog setFilms(List<Film> films) {
        FilmsCatalog filmsCatalog = new FilmsCatalog();
        filmsCatalog.setFilms(films);
        return filmsCatalog;
    }
}
