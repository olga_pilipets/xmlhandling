package application.repository.dao.impl;

import application.repository.dao.GenericDAO;
import org.apache.log4j.Logger;
import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.xml.sax.InputSource;


import java.io.*;


public class GenericDAOImpl<T> implements GenericDAO<T> {

    private static final Logger logger = Logger.getLogger(GenericDAOImpl.class);

    @Override
    public T getInfo(String fileName, String mappingFileName) {
        T info = null;
        try (FileReader fileReader = new FileReader(getClass().getClassLoader().getResource(".").getFile() + fileName)) {
            Mapping mapping = new Mapping();
            mapping.loadMapping(getClass().getClassLoader().getResource(".").getFile() + mappingFileName);
            Unmarshaller unmar = new Unmarshaller(mapping);
            info = (T) unmar.unmarshal(new InputSource(fileReader));
        } catch (Exception e) {
            logger.info("Problems with file");
        }
        return info;
    }

    @Override
    public void addInfo(T info, String fileName, String mappingFileName) {
        try {

            Mapping mapping = new Mapping();
            mapping.loadMapping(getClass().getClassLoader().getResource(".").getFile() + mappingFileName);
            Marshaller marshaller = new Marshaller(new FileWriter(getClass().getClassLoader().getResource(".").getFile() + fileName));
            marshaller.setMapping(mapping);
            marshaller.marshal(info);


        } catch (Exception e) {
            logger.info("Problems with file");
        }
    }
}
