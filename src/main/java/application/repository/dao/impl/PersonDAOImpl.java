package application.repository.dao.impl;

import application.repository.dao.PersonDAO;
import application.repository.model.Person;
import application.repository.model.PersonsList;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("personDAO")
public class PersonDAOImpl extends GenericDAOImpl<Person> implements PersonDAO {
    public List<Person> getPersons(PersonsList personsList) {
        return personsList.getPersons();
    }


}
