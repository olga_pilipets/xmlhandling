package application.repository.dao;


public interface GenericDAO <T> {
    T getInfo(String fileName,String mappingFileName);
    void addInfo(T info,String fileName,String mappingFileName);
}
