package application.repository.dao;

import application.repository.model.Person;
import application.repository.model.PersonsList;

import java.util.List;

public interface PersonDAO{
    List<Person> getPersons(PersonsList personsList);

}
