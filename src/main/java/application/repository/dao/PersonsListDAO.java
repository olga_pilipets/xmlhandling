package application.repository.dao;

import application.repository.model.PersonsList;


public interface PersonsListDAO extends GenericDAO<PersonsList>{
    PersonsList getPersons();
    void setPersons(PersonsList personsList);
}
