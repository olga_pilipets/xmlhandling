package application.repository.dao;

import application.repository.model.FilmsCatalog;


public interface FilmsCatalogDAO extends GenericDAO<FilmsCatalog> {

    FilmsCatalog getFilms();
    void setFilms(FilmsCatalog filmsCatalog);

}